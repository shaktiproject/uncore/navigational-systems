/*
Module name: Top Level Control FSM 
Description: This is the top level control FSM which iterates through differnt stages, namely
			 acquition and tracking. Also, depending on whether the lock detector returns hot start
			 or cold start, this FSM operates accordingly. This module will be interface to the
			 processor.
//TODO 1. Add mechanisms for processor to read the navigational bits. Either one bit at a time, or
//8 or 64 depending upon the granularity required. This can also be parameterised by having a
//register to which the navigational bits are written one after another, and also a counter which
//keeps track of how many bits have been updated in this register.
//Intuitively, we would not require transfer of every bit, as every bit does not convey a messeage.
//Therefore, we could have a register (or register array) of size of data that conveys some
//information. Once this register (or register arrays) has been completely written, this can raise
//an interrupt to the processor. Once control register will also keep live track of how many bits in
//this register (or register array) have been updated.
//TODO 2. Does hot start and warm start require acquisition?
//TODO 3. Add support for multiple tracking channels.
*/
package processingfsm;

    import acquisitionfsm::*;
    import trackingfsm::*;
    import lockdetectorfsm::*;

    interface Ifc_processingfsm;
    endinterface : Ifc_processingfsm
    module mkprocessingfsm(Ifc_processingfsm);

        Ifc_acquisitionfsm ifc1 <- mkacquisitionfsm;
        Ifc_trackingfsm ifc2 <- mktrackingfsm;
        Ifc_lockdetectorfsm ifc3 <- mklockdetectorfsm;

        Reg#(Bit#(20)) rg_loopcnt <- mkReg(2);
        Reg#(FixedPoint#(16,32)) rg_carr_freq <- mkReg(0);
        Reg#(Bit#(22)) rg_code_shift <- mkReg(0);
        Reg#(Int#(33)) rg_last_phase <- mkReg(0);
        Reg#(Bit#(2)) rg_track_status <- mkReg(0);  
        Reg#(Bit#(1)) rg_sign_change <- mkReg(0);   

        Stmt processing_stmt = 
        (seq
            ifc1.getting_started();//Acquisition starts here
            Bit#(1) temp1 = ifc1.prn();
			//temp1 indicates only about one satellite. How do we proceed to acquiring next sat?
            if(temp1 == 1)//If the satellite is acquired
            seq
                action//Getting outputs of acquisition
                    rg_carr_freq <= fromInt(ifc1.freq_shifter);
                    rg_code_shift <= ifc1.code_shifter();
                    rg_last_phase <= 0;
                endaction
                while((rg_track_status != 3)&&(rg_loopcnt<=1500))
                seq
                    if(rg_track_status == 0)
                    begin
                        seq
                            ifc2.acq_inputs(rg_carr_freq,rg_carr_freq,rg_code_shift,rg_last_phase,rg_loopcnt);//Giving inputs to tracking iteration
                            action//Getting outputs of tracking iteration
                                rg_carr_freq <= ifc2.carr_freq_out();
                                rg_code_shift <= ifc2.code_shift_out();
                                rg_last_phase <= ifc2.phase_out();
                                rg_sign_change <= ifc2.sign_change;
                            endaction
                        endseq
                        ifc3.getinputs(rg_loopcnt,rg_sign_change);//Initialisation of Lockdetector FSM
                        rg_track_status <= ifc3.status();//Obtaining the status of tracking
                    end
                    else if(rg_track_status == 1)
                        seq
                            //HOT START IS PERFORMED
                        endseq
                    else if(rg_track_status == 2)
                    seq
                        //WARM START IS PERFORMED
                    endseq
                    if(rg_loopcnt%1500==0)
                    seq
                        //Read the 30s Navigation data from lockdetector FSM
                    endseq
                    rg_loopcnt <= rg_loopcnt + 1;
                endseq
                
            endseq
        endseq);

        FSM processing_fsm <- mkFSM(processing_stmt);

        rule r0_initialise(processing_fsm.done);
            processing_fsm.start();
        endrule

    endmodule : mkprocessingfsm
endpackage : processingfsm
