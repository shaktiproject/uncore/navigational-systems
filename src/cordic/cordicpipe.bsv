package cordicpipe;

import FIFO::*;
import SpecialFIFOs ::*;
import Vector ::*;
import Real ::*;
import FixedPoint::*;
import BRAM::*;
import BRAMCore::*;
import GetPut::*;
import ClientServer::*;

typedef 12 Samples;
typedef 16 Iterations;
typedef 17 Iterations1;//Iterations + 1
typedef 524288 Scale;//Required scalling factor*16

interface Ifc_cordicpipe;
    method Action initialise(Int#(33) base_in, Bit#(20) loopcnt);//Initialisation of cordic table
    method Int#(30) cosine();
    method Int#(30) sine();
    method Int#(33) lastphase_out;
endinterface : Ifc_cordicpipe

module mkcordicpipe(Ifc_cordicpipe);

    Integer samp2 = 12000;
    Integer samp1 = valueOf(Samples);
    Bit#(16) samp = fromInteger(samp1);
    Integer iterations = valueOf(Iterations);
    Bit#(16) cordic_iter = fromInteger(iterations);
    Integer scale = valueOf(Scale);
    Int#(21) scale1 = fromInteger(scale);

    Vector#(31,Reg#(Int#(32))) atan_table <- replicateM(mkReg(0));
    Vector#(Iterations1,FIFO#(Tuple3 #(Int#(34),Int#(34),Int#(32)))) pipe <- replicateM(mkPipelineFIFO);
    FIFO#(Tuple3 #(Int#(12), Int#(12), Int#(33))) f1 <- mkPipelineFIFO;  // alter i and q values
    FIFO#(Tuple3 #(Int#(34), Int#(34), Int#(33))) f2 <- mkPipelineFIFO;  // converge angle within -pi/2 to pi/2;

    Reg#(Bit#(16)) count1 <- mkReg(0);

    BRAM_PORT#(Bit#(36),Int#(12)) i_val <- mkBRAMCore1Load(samp2,False,"/home/sadhana/Desktop/NavIC_correlator/src/i_value",False);
    BRAM_PORT#(Bit#(36),Int#(12)) q_val <- mkBRAMCore1Load(samp2,False,"/home/sadhana/Desktop/NavIC_correlator/src/q_value",False);
    
    Reg#(Bit#(36)) count2 <- mkReg(0);
 
    Reg#(FixedPoint#(1,14)) mult2 <- mkReg(0.60725320630464);
    Reg#(Int#(33)) angle <- mkReg(0);
    Reg#(Int#(33)) angle_base <- mkReg(0);
    Reg#(Bit#(16)) count3 <- mkReg(0);
    Reg#(Bit#(2)) bank_count <- mkReg(0);
    Reg#(Bit#(36)) rg_start_address <- mkReg(0);
    Reg#(Bit#(36)) rg_stop_address <- mkReg(0);

    Reg#(Bit#(3)) rg_readFlag <- mkReg(0);
    Reg#(Bit#(2)) flag <- mkReg(0);
    Reg#(Bit#(1)) readFlag <- mkReg(0);
    Reg#(Bit#(1)) start <- mkReg(0);

    Reg#(Int#(30)) rg_cosine <- mkReg(0);
    Reg#(Int#(30)) rg_sine <- mkReg(0);

    rule r0_get_inputs1(readFlag==1);
        if(count2 == rg_stop_address)
            readFlag <= 0;
        else
        begin
            i_val.put(False,count2,?);
            q_val.put(False,count2,?);
            let temp = angle + angle_base;
            angle<=truncate(temp);
            if(count2 == rg_start_address)
                flag <= 1;
        end
        count2 <= count2+1;
    endrule

    rule r0_get_inputs2(flag == 1);
        let i_in = i_val.read;
        let q_in = q_val.read;
        let angle_in = angle;
        //$display($time, " Input : count2=%d,i_in=%d,q_in=%d and angle_in=%d \n",count2,i_in,q_in,angle_in);
        f1.enq(tuple3(i_in,q_in,angle_in));
        if(count2 == rg_stop_address)
            flag <= 2;
    endrule

    rule r1_add_guards;
        match {.i1, .q1, .a1} = f1.first; f1.deq;
        FixedPoint#(12,1) i2 = fromInt(i1);
        FixedPoint#(12,1) q2 = fromInt(q1);
        FixedPoint#(13,15) i3 = fxptMult(i2,mult2);
        FixedPoint#(13,15) q3 = fxptMult(q2,mult2);
        FixedPoint#(21,1) mult1 = fromInt(scale1);
        Int#(34) i = fxptGetInt(fxptMult(i3,mult1));
        Int#(34) q = fxptGetInt(fxptMult(q3,mult1));
        f2.enq(tuple3(i,q,a1));
    endrule

    rule r2_converge_angle;
        match {.i, .q, .a2} = f2.first; f2.deq;
        Int#(34) i4 = 0;
        Int#(34) q4 = 0;
        Int#(32) a3 = 0;
        if((a2 > 1073741824)&&(a2 <= 2147483648)) //second quadrant
        begin
            i4 = -q;
            q4 = i;
            a3 = truncate(a2 - 1073741824);
        end
        else if((a2 > 2147483648)&&(a2 <= 3221225472)) //third quadrant
                    begin
                        i4 = q;
                        q4 = -i;
                        a3 = truncate(a2 + 1073741824);
                    end
                    else
                        begin //first and fourth quadrant
                            i4 = i;
                            q4 = q;
                            a3 = truncate(a2);
                        end
            pipe[0].enq(tuple3 (i4,q4,a3));
    endrule

    for(Bit#(16) j=0; j<cordic_iter; j=j+1)
    begin
        rule r3_processing;
            match {.x,.y,.z} = pipe[j].first; pipe[j].deq;
            let a2=x >> j;
            let a3=y >> j;
            pipe[j+1].enq(tuple3 (((z>0)?x-a3:x+a3),((z>0)?y+a2:y-a2),((z>0)?z-atan_table[j]:z+atan_table[j])));
        endrule
    end
    rule r4_push_output(count1<samp);
        match{.i1,.q1,.angle1} = pipe[cordic_iter].first;pipe[cordic_iter].deq;
        Int#(30) i_out = truncate(i1>>4);
        Int#(30) q_out = truncate(q1>>4);
        rg_cosine <= i_out;
        rg_sine <= q_out;
        count1 <= count1+1;
        //$display($time,"cordic values : %d %d \n",rg_cosine,rg_sine);
    endrule
    rule r5_next_input(count1 == samp-1);
        start <= 0;
    endrule

    method Action initialise(Int#(33) base_in, Bit#(20) loopcnt) if(start == 0);
//Feedback angles for cordic
        atan_table[00] <= 'b00100000000000000000000000000000; // 45.000 degrees -> atan(2^0)
        atan_table[01] <= 'b00010010111001000000010100011101; // 26.565 degrees -> atan(2^-1)
        atan_table[02] <= 'b00001001111110110011100001011011; // 14.036 degrees -> atan(2^-2)
        atan_table[03] <= 'b00000101000100010001000111010100; // atan(2^-3)
        atan_table[04] <= 'b00000010100010110000110101000011;
        atan_table[05] <= 'b00000001010001011101011111100001;
        atan_table[06] <= 'b00000000101000101111011000011110;
        atan_table[07] <= 'b00000000010100010111110001010101;
        atan_table[08] <= 'b00000000001010001011111001010011;
        atan_table[09] <= 'b00000000000101000101111100101110;
        atan_table[10] <= 'b00000000000010100010111110011000;
        atan_table[11] <= 'b00000000000001010001011111001100;
        atan_table[12] <= 'b00000000000000101000101111100110;
        atan_table[13] <= 'b00000000000000010100010111110011;
        atan_table[14] <= 'b00000000000000001010001011111001;
        atan_table[15] <= 'b00000000000000000101000101111100;
        atan_table[16] <= 'b00000000000000000010100010111110;
        atan_table[17] <= 'b00000000000000000001010001011111;
        atan_table[18] <= 'b00000000000000000000101000101111;
        atan_table[19] <= 'b00000000000000000000010100010111;
        atan_table[20] <= 'b00000000000000000000001010001011;
        atan_table[21] <= 'b00000000000000000000000101000101;
        atan_table[22] <= 'b00000000000000000000000010100010;
        atan_table[23] <= 'b00000000000000000000000001010001;
        atan_table[24] <= 'b00000000000000000000000000101000;
        atan_table[25] <= 'b00000000000000000000000000010100;
        atan_table[26] <= 'b00000000000000000000000000001010;
        atan_table[27] <= 'b00000000000000000000000000000101;
        atan_table[28] <= 'b00000000000000000000000000000010;
        atan_table[29] <= 'b00000000000000000000000000000001;
        atan_table[30] <= 'b00000000000000000000000000000000;
        angle_base <= base_in;
        Bit#(36) temp = zeroExtend(loopcnt-1)*zeroExtend(samp);     //loopcount stores the sample window number (sample window size=12000)
        rg_start_address <= temp;                                   //Used to index BRAM to fetch the right 1ms data window.
        rg_stop_address <= temp + zeroExtend(samp);
        angle <= 0;
        start <= 1;
        readFlag <= 1;
        flag <= 0;
        count1 <= 0;
        count2 <= temp;
        $display($time, " CORDIC started \n");
    endmethod
    method Int#(30) cosine() if(count1 > 0);
        return rg_cosine;
    endmethod
    method Int#(30) sine() if(count1 > 0);
        return rg_sine;
    endmethod
    method Int#(33) lastphase_out if(count1==samp);
        return angle;
    endmethod
endmodule : mkcordicpipe

endpackage